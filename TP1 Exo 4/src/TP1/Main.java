package TP1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Le fichier dans lequel vous voulez écrice");
        String path = sc.nextLine();
        File file = new File(path);
        EcritureDuFichie(file);
    }

    public static void EcritureDuFichie(File fichier) {
        try(FileOutputStream flux = new FileOutputStream(fichier);) {
            String text = "";       // Je creer la variable ext pour pa que ça plante
            Scanner line = new Scanner(System.in);
            System.out.println("Vous pouvez écrire !");
            while (text != "quit") {    // une boucle while infinie pour recuperer du text
                text = line.nextLine();
                if (text != "quit") {

                    String textFinal = text + "\r\n";   //Permet le retour a la ligne
                    for (int i = 0; i < text.length(); i++) {
                        flux.write(textFinal.charAt(i));
                    }
                }
            }
            flux.close();
        }catch (FileNotFoundException e){
        e.printStackTrace();
        }catch (IOException e){
        e.printStackTrace();
         }

    }
}
