package TP1;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Scanner;

public class Main {

    // IMPORTANT : Ce code est valable pour les exercices 2 et 3 car il contient la même base
    // Le filtre pour le type de fichier souhaité a juste était rajouté !

    public static void main(String[] args) {
        System.out.println("Votre chemin pls");
        Scanner sc = new Scanner(System.in);
        String cheminDeBase = sc.nextLine();
        System.out.println("Votre type de fichier pls");
        String sufficeDeFin = sc.nextLine();
        // On Scan le chemin d'acces ainsi que le type de notre fichier souhaiter

        File directory = new File(cheminDeBase);// on creer un file qui est un directory
        File[] files = directory.listFiles(new FilenameFilter() { // on initialise notre tableau files avec comme filtre le suffice de fin qui illustre le type de fichier
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(sufficeDeFin);

            }
        });// puis on affiche le nom des différents fichiers qu'il contient
        for (File filesItem : files
                ) {
           System.out.println(filesItem.getName());

        }



    }
}