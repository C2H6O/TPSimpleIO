package TP1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Fichier que vous voulez copier");
        String pathCopy = sc.nextLine();
        System.out.println("Fichier dans lequel vous voulez écrire ?");
        String pathWrite = sc.nextLine();
        File fileAEcrire = new File("./text2.txt");
        File fileACopier = new File("./text1.txt");
        ReadFile(fileACopier,fileAEcrire);



    }

    public static void WriteFile(String text, File fileWritten) {
        try (FileOutputStream flux = new FileOutputStream(fileWritten);) {

            for (int i = 0; i < text.length(); i++) {
                flux.write(text.charAt(i));
            }
            flux.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public static void ReadFile(File fileCopy,File fileWrite){
        try{
            // a l'aide du scanner, on recupere chaque ligne que l'on envoie dans la
            //fonction WriteFile pour écrire dans le fichier souahité
            Scanner scRead = new Scanner(fileCopy);
            while (scRead.hasNextLine()){
                WriteFile(scRead.nextLine()+"\n",fileWrite);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}